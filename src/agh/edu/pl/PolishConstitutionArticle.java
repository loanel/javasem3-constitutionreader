
package agh.edu.pl;
import java.util.ArrayList;
import java.util.List;
public class PolishConstitutionArticle implements Article {
	String articleText ="";
	private int number;
	public PolishConstitutionArticle(int number){
		this.number = number;
	}
	@Override
	public int getNumber() {
		
		return this.number;
	}

	@Override
	public void addParagraph(String paragraph) {
		this.articleText = this.articleText + paragraph;
	}
	
	public String toString(){
		return articleText;
	}

}
