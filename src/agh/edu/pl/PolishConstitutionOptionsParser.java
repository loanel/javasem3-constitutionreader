package agh.edu.pl;

import java.io.IOException;

public class PolishConstitutionOptionsParser implements OptionsParser {

	@Override
	public String parseOptions(String[] options) {
		CParser parser = new PolishConstitutionParser();
		try{
			Constitution konstytucja = parser.buildConstitution(options[0]);
			if (options.length == 3){
				if(options[1].equals("A")){
					int number = Integer.parseInt(options[2]);
					return konstytucja.printArticle(number);
				}
				else if(options[1].equals("C")){
					int number = Integer.parseInt(options[2]);
					return konstytucja.printChapter(number);
				}
					else  throw new IllegalArgumentException("Suspected wrong character, A for Article, C for Chapter instead");
			}
			else if(options.length == 4){
					if(options[1].equals("A")){
						int first = Integer.parseInt(options[2]);
						int last = Integer.parseInt(options[3]);
						return konstytucja.printArticles(first, last);
					}
					else  throw new IllegalArgumentException("Suspected wrong character, A for Article, C for Chapter instead");
				}
			else throw new IllegalArgumentException("Suspected wrong amount of arguments");
		}
		catch (IOException e){
			System.out.println(e);
		}
		catch (IllegalArgumentException e){
			System.out.println(e);
		}
		return null;
	}
}	



