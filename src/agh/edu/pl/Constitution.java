package agh.edu.pl;



public interface Constitution {	
		// kazdy reprezentant tej klasy posiada ArrayListe Chapterow i ArrayListe artyku��w ( artyku�y dla szybszego dzia�ania metody wypisujacej artyku�y)
		// parsowaniem pliku zajmuje sie obiekt z klasy CParser. On inicjuje cala zawartosc konstytucji
		
		void addArticle(Article article);
		void addChapter(Chapter chapter);

		// wypisanie artyku�u o danym numerze lub zakresu artyku��w
		String printArticle(int number);
		String printArticles(int first, int last);
		
		// wypisanie danego rozdzialu
		String printChapter(int number);
		
}
