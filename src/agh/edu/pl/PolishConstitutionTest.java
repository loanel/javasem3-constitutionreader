
package agh.edu.pl;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.*;
import java.io.IOException;
public class PolishConstitutionTest {
	
	CParser parser;
	Constitution konstytucja;
	@Before
	public void initObject() {
		try{
			parser = new PolishConstitutionParser();
			konstytucja = parser.buildConstitution("konstytucja.txt");
		}
		catch (IOException e){
			System.out.println(e);
		}
	}
	
	@Test
	public void printArticleTest(){
		assertEquals("Art.5\nRzeczpospolita Polska strze�e niepodleg�o�ci i nienaruszalno�ci swojego terytorium,\nzapewnia wolno�ci i prawa cz�owieka i obywatela oraz bezpiecze�stwo obywateli,\nstrze�e dziedzictwa narodowego oraz zapewnia ochron� �rodowiska, kieruj�c si�\nzasad� zr�wnowa�onego rozwoju.\n",
				konstytucja.printArticle(5));
		assertEquals("Art.1\nRzeczpospolita Polska jest dobrem wsp�lnym wszystkich obywateli.\n", konstytucja.printArticle(1));
		assertEquals("Art.18\nMa��e�stwo jako zwi�zek kobiety i m�czyzny, rodzina, macierzy�stwo i rodzicielstwo\nznajduj� si� pod ochron� i opiek� Rzeczypospolitej Polskiej.\n",
				konstytucja.printArticle(18));
	}
	
	public void printArticlesTest(){
		assertEquals("Art.11\n1. Rzeczpospolita Polska zapewnia wolno�� tworzenia i dzia�ania partii politycznych.\nPartie polityczne zrzeszaj� na zasadach dobrowolno�ci i r�wno�ci obywateli\npolskich w celu wp�ywania metodami demokratycznymi na kszta�towanie\npolityki pa�stwa.\n2. Finansowanie partii politycznych jest jawne.\nArt.12\nRzeczpospolita Polska zapewnia wolno�� tworzenia i dzia�ania zwi�zk�w zawodowych,\norganizacji spo�eczno-zawodowych rolnik�w, stowarzysze�, ruch�w obywatelskich,\ninnych dobrowolnych zrzesze� oraz fundacji.\n",
				konstytucja.printArticles(11, 12));
	}

}

