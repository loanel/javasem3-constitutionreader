package agh.edu.pl;
import java.io.IOException;

public interface CParser {
	// metoda zawierajaca implementacja parsera pliku, usuwa entery/znaki konca lini/junk/etc i tworzy konstytucje
	//? podrozdzialy
	//? czy zrobic ConstitutionBuilder
	
	Constitution buildConstitution(String file_path) throws IOException;
	
}
