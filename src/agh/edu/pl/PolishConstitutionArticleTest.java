package agh.edu.pl;

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.*;

public class PolishConstitutionArticleTest {
	private Article article;
	@Before
	public void initObject(){
		article = new PolishConstitutionArticle(100);
		article.addParagraph("Testowy podpunkt");
	}
	
	@Test
	public void numberTest() {
		assertEquals(100, article.getNumber());
	}
	
	@Test
	public void testParagraph() {
		assertEquals("Testowy podpunkt", article.toString());
	}
	
}
