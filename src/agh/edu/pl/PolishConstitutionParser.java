package agh.edu.pl;

import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.lang.StringBuilder;
import java.lang.NullPointerException;

public class PolishConstitutionParser implements CParser {

	@Override
	public Constitution buildConstitution(String file_path) throws IOException{
		//inicjalizacja pliku i readera
		try(BufferedReader br = new BufferedReader(new FileReader(file_path))){	
			//inicjalizacja zmiennych
			Constitution polishconst = new PolishConstitution();
			int articleCounter = 0;
			int chapterCounter = 0;
			Chapter currentChapter = null;
			Article currentArticle = null;
			boolean startAddingText = false;
			boolean pauseFixReq = false;
			// budowanie strukextury klas
			String inputText;
			String splitter[];
			while((inputText = br.readLine()) != null){
				if(inputText.length()>1 && !inputText.matches("[0-9][0-9][0-9][0-9]-[0-9][0-9]-[0-9][0-9]") && !inputText.matches("�Kancelaria Sejmu") && !(Character.isUpperCase(inputText.charAt(0)) && Character.isUpperCase(inputText.charAt(1)))){ // odrzucamy 
					StringBuilder newText = new StringBuilder(inputText);
					splitter = inputText.split(" ", 2);
					switch (splitter[0]){
						case "Rozdzia�":  currentChapter = new PolishConstitutionChapter(chapterCounter + 1);
										  polishconst.addChapter(currentChapter);
										  startAddingText = true; // dodawanie nowego tekstu rozpoczynamy od pierwszego napotkanego rozdzialu w danej konstytucji
										  chapterCounter++;
										  break;
						case "Art.":  currentArticle = new PolishConstitutionArticle(articleCounter + 1);
									  polishconst.addArticle(currentArticle);
									  currentChapter.addArticle(currentArticle);
									  articleCounter++;
									  break;
						default : if(pauseFixReq){
									  newText = fixText(newText); // zamieniamy pierwsz� spacj� nowej linii na \n
									  pauseFixReq = false;
								  }
								  if(newText.charAt(newText.length()-1) == '-') { // odkrylismy "-" na koncu linii 
									  newText.setLength(newText.length()-1); // usuwam "-" z tekstu
									  pauseFixReq = true; // ustawiam flage wymagania naprawy tekstu
								  }
								  if(!pauseFixReq) newText.append("\n"); // zawsze dodajemy znak \n, o ile nie musimy naprawic struktury tekstu
								  if(startAddingText) currentArticle.addParagraph(newText.toString());
					}
				}	
			}
			return polishconst;
		}
		catch (NullPointerException e){
			System.out.println(e);
		}
		catch (ArrayIndexOutOfBoundsException e){
			System.out.println(e);
		}
		return null;
	}
	public StringBuilder fixText(StringBuilder newText){
		boolean pauseOccured = false;
		  int i=0;
		  while(!pauseOccured && i<newText.length()){
			  if(newText.charAt(i) == ' '){
				  pauseOccured = true;
				  newText.setCharAt(i, '\n');  // znajdujemy pierwsza spacje w nowej lini i zamieniamy ja na \n
			  }
			  i++;
		  }
		return newText;
		
	}

}
