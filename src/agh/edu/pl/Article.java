package agh.edu.pl;

public interface Article {
	// wypisuje tre�� artyku�u
	String toString();
	// wypisuje numer artyku�u
	int getNumber();
	// dodanie paragrafu do listy stringow artyku�u
	void addParagraph(String paragraph);
}
