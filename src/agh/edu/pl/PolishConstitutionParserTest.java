
package agh.edu.pl;

import static org.junit.Assert.*;
import java.io.IOException;

import org.junit.Test;
import org.junit.*;
public class PolishConstitutionParserTest {

	PolishConstitutionParser parser;
	@Before
	public void initObject() {
		parser = new PolishConstitutionParser();
	}
	@Test
	public void fixTest(){
		StringBuilder a = new StringBuilder();
		a.append("Rzeczpospolita Polska");
		assertEquals("Rzeczpospolita\nPolska", parser.fixText(a).toString());
	}
	

}
