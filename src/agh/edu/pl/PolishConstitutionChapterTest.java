package agh.edu.pl;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.*;
public class PolishConstitutionChapterTest {

	private Chapter chapter;
	@Before
	public void initObject() {
		chapter = new PolishConstitutionChapter(10);
		PolishConstitutionArticle a = new PolishConstitutionArticle(10);
		a.addParagraph("Test rozdzialu");
		chapter.addArticle(a);
	}
	@Test
	public void numberTest(){
		assertEquals(10, chapter.getNumber());
	}
	
	@Test
	public void textTest(){
		assertEquals("Art.10\nTest rozdzialu", chapter.toString());
	}
}
